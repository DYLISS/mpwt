import sys

from mpwt.multipwt import run

def main(args=None):
    run()

if __name__ == "__main__":
    main()
